import { Title } from '@angular/platform-browser';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ObservableMedia } from '@angular/flex-layout';
import { finalize } from 'rxjs/operators';

import { I18nService } from '@app/core';
import { SwapiService } from '@app/home/swapi.service';

@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss']
})
export class ShellComponent implements OnInit {

  planets: any;
  isLoading: boolean;

  constructor(private router: Router,
              private titleService: Title,
              private media: ObservableMedia,
              private i18nService: I18nService,
              private swapiService: SwapiService) { }

  ngOnInit() {
    this.isLoading = true;
    this.swapiService.getAllPlanets()
      .pipe(finalize(() => { this.isLoading = false; }))
      .subscribe((res: any) => {
        this.planets = res;
      });
  }

  goTo(name: string) {
    this.isLoading = true;
    this.router.navigate(['/planet', name.toLocaleLowerCase()]);

  }

  setLanguage(language: string) {
    this.i18nService.language = language;
  }

  get languages(): string[] {
    return this.i18nService.supportedLanguages;
  }

  get isMobile(): boolean {
    return this.media.isActive('xs') || this.media.isActive('sm');
  }

  get title(): string {
    return this.titleService.getTitle();
  }
}
