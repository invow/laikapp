import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { SharedModule } from '@app/shared';
import { MaterialModule } from '@app/material.module';
import { PlanetComponent } from '@app/planet/planet.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ActivatedRoute } from '@angular/router';
import { SwapiService } from '@app/home/swapi.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';


describe('PlanetComponent', () => {
  let component: PlanetComponent;
  let fixture: ComponentFixture<PlanetComponent>;
  const fakeActivatedRoute = {
    snapshot: { data: { } }
  } as ActivatedRoute;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
        imports: [
          BrowserAnimationsModule,
          FlexLayoutModule,
          MaterialModule,
          SharedModule,
          NgxDatatableModule,
          HttpClientTestingModule
        ],
        declarations: [PlanetComponent],
        providers: [SwapiService, {provide: ActivatedRoute, useValue: fakeActivatedRoute}]
      })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
