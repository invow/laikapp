import { Component, OnInit, } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SwapiService } from '@app/home/swapi.service';
import { finalize } from 'rxjs/operators';
import { Alert } from 'selenium-webdriver';

@Component({
  selector: 'app-planet',
  templateUrl: './planet.component.html',
  styleUrls: ['./planet.component.scss']
})
export class PlanetComponent implements OnInit {
  planet: any;
  planets: any;
  key: string;
  isLoading: boolean;

  rows: Array<any>;
  columns = [
    { name: 'Name' },
    { name: 'Height' },
    { name: 'Mass' },
    { name: 'Gender' }
  ];

  constructor(private route: ActivatedRoute,
              private swapiService: SwapiService) { }

  ngOnInit() {
    this.key = 'name';
    this.isLoading = true;
    this.swapiService.getAllPlanets().subscribe((res: Object) => {
        this.planets = res;
        this.route.params.subscribe(params => {
          this.rows = [];
          if (params && params.name) {
            for (let i = 0; i < this.planets.results.length; i++) {
                if (this.planets.results[i]['name']
                    .toLocaleLowerCase()
                    .indexOf(params.name) !== -1) {
                  this.swapiService.getItem(
                    this.planets.results[i].url)
                  .pipe(finalize(() => {
                    this.isLoading = false;
                  }))
                  .subscribe((planetRes: Object) => {
                    this.planet = planetRes;
                    if (this.planet && this.planet.residents && this.planet.residents.length !== 0) {
                      this.isLoading = true;
                      this.planet.residents.forEach((element: string) => {
                        this.swapiService.getItem(element)
                          .subscribe((residentRes: any) => {
                            this.isLoading = false;
                            this.rows.push(residentRes);
                          });
                      });
                    }
                  });
                }
            }
          }
        });
      });
  }
}
