import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MaterialModule } from '@app/material.module';
import { PlanetRoutingModule } from './planet-routing.module';
import { PlanetComponent } from '@app/planet/planet.component';
import { SwapiService } from '@app/home/swapi.service';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SharedModule } from '@app/shared';

@NgModule({
  imports: [
    NgxDatatableModule,
    CommonModule,
    TranslateModule,
    FlexLayoutModule,
    MaterialModule,
    PlanetRoutingModule,
    SharedModule
  ],
  declarations: [
    PlanetComponent
  ],
  providers: [
    SwapiService
  ]
})
export class PlanetModule { }
