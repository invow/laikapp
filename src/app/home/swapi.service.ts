import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';

const routes = {
  quote: () => `planets`
};

@Injectable()
export class SwapiService {

  constructor(private httpClient: HttpClient) { }

  getAllPlanets(): Observable<any> {
    return this.httpClient
      .get('planets');
  }

  getItem(endpoint: string): Observable<any> {
    return this.httpClient
      .get(endpoint);
  }
}
