import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';

import { SwapiService } from '@app/home/swapi.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  planets: Object;
  isLoading: boolean;

  constructor(private swapiService: SwapiService) { }

  ngOnInit() {
    this.isLoading = false;
    this.swapiService.getAllPlanets()
      .pipe(finalize(() => { this.isLoading = false; }))
      .subscribe((res: Object) => {
        this.planets = res;
      });
  }

}
